#! /bin/bash

# Overload this env-var to pull from any other registry
DOCKER_REGISTRY=${DOCKER_REGISTRY-"docker.io"}
echo "Docker version of $(echo $0 | sed -e 's/.*\///')"
echo

docker run -it --rm \
     ${DOCKER_REGISTRY}/mambrus/docker-mambrus-my-projects \
    lua-help.lua
