.DEFAULT_GOAL := image

CURRENT_DIR := $(shell pwd)
DOCKER_REGISTRY ?= ${USER}
REPO ?= $(shell basename `pwd`)-$(subst * ,,$(shell git branch | grep -Ee '^\*'))
VERSION := 0.0.1

.PHONY: run
run: image
	docker run -it --rm \
      ${DOCKER_REGISTRY}/${REPO} \
      /start.sh

launch: image
	docker run -itd --restart=always \
      ${DOCKER_REGISTRY}/${REPO} \
      /start.sh > launch
	@echo "Attach to container: docker attach $(shell cat launch)"

attach: launch
	docker attach $(shell cat launch)

build/ambrus.gpg:
	gpg --export 09720B4FF638DDBB067A2FCEBAB71F44F6DB46CD > build/ambrus.gpg

image: build/ambrus.gpg
	docker build -t ${DOCKER_REGISTRY}/${REPO} build
	docker image list | grep ${DOCKER_REGISTRY}\/${REPO} > image

.PHONY: publish
publish: publish-latest publish-version

.PHONY: publish-latest
publish-latest: tag-latest ## Publish the `latest` tagged
	@echo 'publish latest to $(DOCKER_REGISTRY)/$(REPO)'
	docker push $(DOCKER_REGISTRY)/$(REPO):latest

.PHONY: publish-version
publish-version: tag-version ## Publish the `{version}` tagged
	@echo 'publish $(VERSION) to $(DOCKER_REGISTRY)'
	docker push $(DOCKER_REGISTRY)/$(REPO):$(VERSION)

# Docker tagging
.PHONY: tag-latest
tag: tag-latest tag-version ## Generate container tags for the `{version}` and `latest` tags

.PHONY: tag-latest
tag-latest: image ## Generate container `{version}` tag
	@echo 'create tag latest'
	docker tag $(DOCKER_REGISTRY)/$(REPO) $(DOCKER_REGISTRY)/$(REPO):latest

.PHONY: image tag-version
tag-version: ## Generate container `latest` tag
	@echo 'create tag $(VERSION)'
	docker tag $(DOCKER_REGISTRY)/$(REPO) $(DOCKER_REGISTRY)/$(REPO):$(VERSION)

.PHONY: clean
clean:
	-docker stop $(shell cat launch)
	-docker rm $(shell cat launch)
	-rm launch
	-docker image remove ${DOCKER_REGISTRY}/${REPO}
	-rm image
	-rm build/ambrus.gpg

.PHONY: info
info:
	@echo
	@echo "REPO:"
	@echo ${REPO}
	@echo
	@echo "image:"
	@echo $(shell cat image)
	@echo
	@echo "launch:"
	@echo $(shell cat launch)
	@echo
	@echo "Container:"
	@echo $(DOCKER_REGISTRY)/$(REPO) $(REPO):latest
	@echo
	
