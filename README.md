# docker-mambrus

This is a simplistic docker-base project for the main distribution I'm
currently providing packages for. I.e. you can use it to install packages
from my binary repos for any of my projects without risking any
contamination your host.

Branches contain builds and initializations for pre-built examples. There
should not be too many as there is no point to <i>"divide and conquer"</i>
more than necessary. But the branch-names may not be self-explanatory,
therefore an index will be kept in this README.

## Branch index

| Branch-name       | Containing                                        |
| ----------------- | ------------------------------------------------- |
| master            | Simplistic Bullseye-build with my apt-repo added  |
| my-projects       | My projects included in build and wrappers using them as if installed |

## Build
* Clone and check-out the branch you intend to base your container build on
* `make`
* `make tag`
* `make publish`

### pihole quirks

If your `NAT-router/fire-wall` uses
[pihole](https://github.com/pi-hole/docker-pi-hole) on a container running
at the same host as you build, you may want to add `"dns": ["10.1.2.3",
"8.8.8.8"]` to your `/etc/docker/daemon.json` in case apt update/upgrade
fails when you build. I.e. something like this:

``` bash
$ cat /etc/docker/daemon.json
{
        "experimental": true,
        "dns": ["10.1.2.3", "8.8.8.8"]
}
```

**Note:** Do not change your NAT-router for docker-builds as you'd only want to change
it back later with disruptive `NAT` reboots in-between.


## Pre-builds on `$DOCKER_REGISTRY`

Default registry is `docker.io`

* This image including my projects pre-installed and ready to run using
  the shell wrappers in this project (see script snippets under
  [`bin/`](https://gitlab.com/mambrus/docker-mambrus/-/tree/master/bin?ref_type=heads)
  in this project):
  [docker-mambrus-my-projects](https://hub.docker.com/repository/docker/mambrus/docker-mambrus-my-projects/general)
* This bare image pre-built with only my apt-repo added:
  [docker-mambrus-master](https://hub.docker.com/repository/docker/mambrus/docker-mambrus-master/general)

## Run my projects as if installed

Clone this repo for the wrapper scripts. They will run a docker container
with the application as arguments and some minor environments and instance
settings.You're encouraged to open and read them before running
[`bin/`](https://gitlab.com/mambrus/docker-mambrus/-/tree/master/bin?ref_type=heads)

``` bash
git clone https://gitlab.com/mambrus/docker-mambrus
cd docker-mambrus
source .env
mycalc
```

<i>(Replace last-line with any name in the
[`bin/`](https://gitlab.com/mambrus/docker-mambrus/-/tree/master/bin?ref_type=heads) directory)</i>

End-result for the example above should be:

```
$ mycalc
Docker version of mycalc

Unable to find image 'mambrus/docker-mambrus-my-projects:latest' locally
latest: Pulling from mambrus/docker-mambrus-my-projects
Digest: sha256:270ae4c92976ec38fc4e0441a56ce5c56e5e9ebd03ce4fb0c0aa8b2d67b7f844
Status: Downloaded newer image for mambrus/docker-mambrus-my-projects:latest

Welcome to mycalc version 0.9.20-675b3b7
Expression results are stored in register 'R'        [DEG] [NRM]
> sin(30)
0.5
>
```

**Note:** The pull-text will only be output first time for any of the
wrappers. Consecutive runs are basically as seamless and as fast to start as
if each project was installed natively.

## Browse this wiki locally

``` bash
apt install gitit
git clone https://github.com/helsinova/wiki-wrapper.git
cd wiki-wrapper
rm wikidata
ln -s local/path/to/docker-ambrus wikidata
./start_wiki.sh
```

## Contact

* Michael Ambrus , Author [keybase link](https://keybase.io/mambrus)
* Use my [Public key](https://keybase.io/mambrus/pgp_keys.asc?fingerprint=09720b4ff638ddbb067a2fcebab71f44f6db46cd)
  to verify commits, contact etc.
